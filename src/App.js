import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";

import Antwoorden from "./components/antwoorden.component";
import VragenLijst from "./components/vragen-lijst.component";
import CreateVraag from './components/create-vraag.component';
import MijnVragen from './components/mijnvragen-lijst.component';
import Vraag from './components/vraag.component';



// GEEN VERIFICATIE
// locale user ID zonder verificatie om te kijken wie de ingelogd is
localStorage.setItem('userToken', '5ef0b9f62e57a1b3d01d7cc6');
localStorage.setItem('userName', 'Max Dobbelsteen');

function App() {

  return (
    <Router>
      <Route path="/vragen" exact component={VragenLijst} />
      <Route path="/create" component={CreateVraag} />
      <Route path="/mijnvragen" component={MijnVragen} />
      <Route path="/vraag/:id" component={Vraag} />
      <Route path="/antwoorden/:id"  component={Antwoorden} />
    </Router>
  );
}

export default App;
