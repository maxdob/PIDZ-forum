import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Navbar from "./navbar.component";

//styling of each question
const MijnVraag = props => (
    <div className="container">
        <div>
            <div className="question-name">
                <b>{props.vraag.username}</b> heeft een vraag
            </div>
            <h1 className="question-title">
                {props.vraag.title}
            </h1>
            <Link to={"/vraag/"+props.vraag._id}>edit</Link>
        </div>
    </div>
  )

export default class MijnVragenLijst extends Component {

    constructor(props) {
        super(props);

        this.state = {mijnvragen: []};
    }

    // Gets questions from DB and puts them in an array
    componentDidMount() {
        axios.get('http://localhost:5000/users/' + localStorage.getItem('userToken')).then(response => {
                this.setState(response.data)        
        })
        .catch((error) => {
            console.log(error);
        })
    }

    // Maps through all question from the array
    mijnVragenLijst() {
        return this.state.mijnvragen.forEach(currentvraag => 
            axios.get('http://localhost:5000/vragen/' + currentvraag).then(response => {
                return console.log(response.data)
            })
        )
     }



    render() {
        return (
            <div>
                <Navbar initialValue={"Vragen Forum"} search={true} category={true} />
                { this.mijnVragenLijst() }
            </div>
        )
    }
}