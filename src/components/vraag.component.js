import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';

import Navbar from "./navbar.component";

import { ReactComponent as SaveIcon } from '../icons/save.svg';
import { ReactComponent as MoreIcon } from '../icons/settings.svg';
import { ReactComponent as PencilIcon } from '../icons/pencil.svg';


//Component for al questions
const Comment = props => (
    <div className="container question-box">
        <div className="question-name">
            { props.vraag.anonymous ? 'Iemand' : props.vraag.name } reageert
        </div>
        <div className="font-sm mt-1">
            {props.vraag.message}
        </div>
    </div>
  )



export default class Vraag extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            userid: '',
            name: '',
            title: '',
            description: '',
            anonymous: false,
            date: '',
            comments: [],
            label: ' reacties'
        };
    }

    //Gets question info from DB and puts them in states
    componentDidMount() {
        axios.get('http://localhost:5000/vragen/'+ this.props.match.params.id).then(response => {
            this.setState({
                userid: response.data._id,
                name: response.data.name,
                title: response.data.title,
                description: response.data.description,
                date: response.data.date,
                anonymous: response.data.anonymous,
                comments: response.data.comments
            })
        })
        .catch((error) => {
            console.log(error);
        })
    }

    commentLijst() {

        return this.state.comments.slice(0).reverse().map(currentcomment => {
            return <Comment vraag={currentcomment}  key={currentcomment._id}/>;
          })
        }

    render() {
        console.log(this.state.comments)
        return (
            <div>
                <Navbar initialValue={"Vraag"} search={false} category={false}/>
                <div className="container mt-3 ">
                    <div className="font-sm">{ this.state.anonymous ? 'Iemand' : <b>{this.state.name}</b> } heeft een vraag</div>
                    <div className="font-lr mt-1"> {this.state.title}</div>
                    <div className="font-rg mt-1">{this.state.description}</div>
                    <div className="d-flex mt-4">
                    <div className="bg-main py-1 rounded">
                        <PencilIcon className="icon-question m-2 ml-3" />
                        <NavLink className="font-question-white mr-3" to={"/antwoorden/"+this.props.match.params.id}>Antwoorden</NavLink>
                    </div>
                    <div className="mt-2 ml-4">
                        <SaveIcon className="icon-question" />
                        <NavLink className="font-question" to={"/vraag/"}>Opslaan</NavLink>
                    </div>
                    <div className="ml-auto mt-2">
                        <MoreIcon className="icon-question" />
                    </div>
                </div>
                </div>
                <div className="bg-sec  mt-3 py-1">
                    <div className="container d-flex">
                        <div>
                            {this.state.comments.length} {this.state.comments.length === 1 ? ' reactie' : ' reacties'}
                        </div>
                        
                    </div>
                </div>
                { this.commentLijst() }
                
            </div>
        );
    }
}