import React, { Component } from 'react';
import axios from 'axios';

import Navbar from "./navbar.component";

export default class CreateVraag extends Component {
    constructor(props) {
        super(props);
        
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeAnonymous = this.onChangeAnonymous.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        this.state = {
            name: localStorage.getItem('userName'),
            title: '',
            description: '',
            date: date,
            anonymous: false,
            views: 0
        }
    }
    
    componentDidMount() {
        this.setState({
            username: 'test user'
        });
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }


    onChangeAnonymous(e) {
        this.setState({
            anonymous: !this.state.anonymous
          })
          
    }

    onSubmit(e) {
        e.preventDefault();
        const vraag = {
            name: this.state.name,
            title: this.state.title,
            description: this.state.description,
            date: this.state.date,
            anonymous: this.state.anonymous,
            views: this.state.views,
            comments: [null]
        }
        console.log(vraag)

        //POST vraag naar database
        axios.post('http://localhost:5000/vragen/add', vraag)
        .then(res => console.log(res.data));
    }

    render() {
        return (
        <div>
            <Navbar initialValue={"Stel een vraag"} />
        
            <div className="container">
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                    </div>
                    <div className="form-group"> 
                    <label>Titel: </label>
                    <textarea  type="text"
                        rows="2"
                        required
                        className="form-control "
                        placeholder="test"
                        value={this.state.title}
                        onChange={this.onChangeTitle}
                        />
                    </div>
                    <div className="form-group">
                    <label>Description: </label>
                    <textarea type="text"
                        rows="2"
                        required
                        placeholder="test"
                        className="form-control"
                        value={this.state.description}
                        onChange={this.onChangeDescription}
                        />
                    </div>
                    <div className="form-group">
                
                    <div className="custom-control custom-switch">
                        <input type="checkbox" className="form-control custom-control-input" checked={this.state.anonymous} onChange={this.onChangeAnonymous} id="customSwitch1" />
                        <label className="custom-control-label" for="customSwitch1">Anoniem blijven</label>
                    </div>
                        </div>
                    <div className="form-group">
                    <input type="submit" value="Plaats vraag" className="btn btn-primary" />
                    </div>
                </form>
            </div>
            </div>
        )
    }
}