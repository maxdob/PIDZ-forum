import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';

import Navbar from "./navbar.component";

import { ReactComponent as EyeIcon } from '../icons/eye.svg';
import { ReactComponent as SaveIcon } from '../icons/save.svg';
import { ReactComponent as MoreIcon } from '../icons/settings.svg';

//Component element for each question
const Vraag = props => (
    <div className="container question-box">
            <div className="question-name">
                <b>{props.vraag.name}</b> heeft een vraag
            </div>
            <h1 className="question-title">
                {props.vraag.title}
            </h1>
            <div className="question-name">
                {props.vraag.comments.length}
                {props.vraag.comments.length === 1 ? ' reactie' : ' reacties'}
            </div>
            <div className="d-flex mt-2">
                <div>
                    <EyeIcon className="icon-question" />
                    <NavLink className="font-question" to={"/vraag/"+props.vraag._id}>Bekijk vraag</NavLink>
                </div>
                <div className="ml-4">
                    <SaveIcon className="icon-question" />
                    <NavLink className="font-question" to={"/vraag/"+props.vraag._id}>Opslaan</NavLink>
                </div>
                <div className="ml-auto">
                    <MoreIcon className="icon-question" />
                </div>
            </div>
    </div>
  )

export default class VragenLijst extends Component {

    constructor(props) {
        super(props);

        this.state = {vragen: []};
    }

    //Gets questions from DB and puts them in an array
    componentDidMount() {
        axios.get('http://localhost:5000/vragen/').then(response => {
            this.setState({vragen : response.data})
        })
        .catch((error) => {
            console.log(error);
        })
    }

    // Maps through all question from the array
    vragenLijst() {
        return this.state.vragen.slice(0).reverse().map(currentvraag => {
          return <Vraag vraag={currentvraag}  key={currentvraag._id}/>;
        })
      }

    render() {
        return (
            <div>
                <Navbar initialValue={"Vragen Forum"} search={true} category={true}/>
            
                { this.vragenLijst() }
            </div>
        );
    }
}