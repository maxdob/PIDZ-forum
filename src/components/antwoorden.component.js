import React, { Component } from 'react';
import axios from 'axios';

import Navbar from "./navbar.component";

import { ReactComponent as SendIcon } from '../icons/send.svg';
import { NavLink } from 'react-router-dom';



export default class Antwoorden extends Component {
    constructor(props) {
        super(props);
        
        this.onChangeAnswer= this.onChangeAnswer.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            userid: '',
            name: '',
            title: '',
            description: '',
            date: '',
            anonymous: false,
            comments: [],
            answer: '',
            anonymousanswer: false
        };
    }

    onChangeAnswer(e) {
        this.setState({
            answer: e.target.value
        });
    }

    onChangeAnonymous(e) {
        this.setState({
            anonymousanswer: e.target.value
        });
    }

    //Gets question info from DB and puts them in states
    componentDidMount() {
        axios.get('http://localhost:5000/vragen/'+ this.props.match.params.id).then(response => {
            this.setState({
                userid: response.data._id,
                name: response.data.name,
                title: response.data.title,
                description: response.data.description,
                date: response.data.date,
                anonymous: response.data.anonymous,
                comments: response.data.comments
            })
        })
    }


    onSubmit(e) {
        e.preventDefault();

        const newComment = {
            'user_id': localStorage.getItem('userToken'),
            'name': localStorage.getItem('userName'),
            'message': this.state.answer,
            'anonymous': this.state.anonymousanswer
        }

        const newarray = this.state.comments;

        newarray.push(newComment);

        this.setState({
            comments: newarray
        });

        const vraag = {
            name: this.state.name,
            title: this.state.title,
            description: this.state.description,
            date: this.state.date,
            anonymous: this.state.anonymous,
            views: this.state.views,
            comments: this.state.comments
        }
        
        //POST add comment to the vraag
        axios.post('http://localhost:5000/vragen/update/'+ this.props.match.params.id, vraag)
        .then(res => console.log(res.data));

                this.props.history.push('/vraag/' + this.props.match.params.id)
        
        
    }



    render() {
        return (
        <div>
            <Navbar initialValue={"Antwoorden"} />
                <div className="container question-answer font-me">
                    { this.state.title}
                </div>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group m-3"> 
                    <textarea rows="8"
                        type="text"
                        placeholder="Antwoord op de gestelde vraag"
                        required
                        autoFocus
                        className="w-100"
                        value={this.state.answer}
                        onChange={this.onChangeAnswer}
                        />
                    </div>
                
                    <div className="form-group d-flex top-border answer-question">
                        <div className="custom-control custom-switch my-2 ml-3">
                            <input type="checkbox" className="form-control custom-control-input" checked={this.state.anonymousanswer} onChange={this.onChangeAnonymous} id="customSwitch1" />
                            <label className="custom-control-label" for="customSwitch1">Anoniem reageren</label>
                        </div>
                        <button type="submit" value="Plaats vraag" className="bg-main send-comment ml-auto"><SendIcon className="my-1" /></button>
                    </div>
                </form>
            </div>
        )
    }
}