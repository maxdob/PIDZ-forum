import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../App.css';

import { ReactComponent as AddIcon } from '../icons/add.svg';
import { ReactComponent as FilterIcon } from '../icons/filter.svg';
import { ReactComponent as NotificationIcon } from '../icons/notification.svg';
import { ReactComponent as BackIcon } from '../icons/back.svg';

export default class Navbar extends Component {

    constructor(props) {
        super(props);
     
        this.state = {
          title: props.initialValue,
          search: props.search,
          category: props.category,
          backbutton: false
        };
    }

    

    render() {

        return (
            <div className="bg-main">
                <div className="topnav">
                    <div className="container d-flex">
                        { this.state.category && this.state.search ? null :
                        <NavLink to={"/vragen/"}><BackIcon  className="mt-1 mr-3"/></NavLink>
                        }
                        <h4 className="white-f">{this.state.title}</h4>
                        <NotificationIcon className="ml-auto filter-button" />
                    </div>
                </div>
                { this.state.search ? 
                    <div className="container py-2 d-flex" id="search-add-nav">
                        <input className="search-bar mr-4 flex-fill" type="text" placeholder="Zoek groep, vraag, onderwerp..." aria-label="Search"></input>
                        <div className="ml-auto add-btn bg-white">
                            <NavLink to="/create"> <AddIcon to="/create" className="vertical-center"/></NavLink> 
                        </div>
                    </div>
                : null }
                { this.state.category ? 
                    <div className="container" id="questions-nav">
                        <div className="vragen-nav d-flex pb-2">
                            <NavLink to="/vragen" activeStyle={{ fontWeight: 'bold', color: '#fff' }} className="nav-links m-0">Alle vragen</NavLink>
                            <NavLink to="/mijnvragen" activeStyle={{ fontWeight: 'bold', color: '#fff' }} className="nav-links">Mijn vragen</NavLink>
                            <NavLink to="/opgeslagen"  activeStyle={{ fontWeight: 'bold', color: '#fff' }} className="nav-links">Opgeslagen</NavLink>
                            <FilterIcon  className="ml-auto filter-button" />
                        </div>
                    </div>
                : null  }
            </div>  
        )
    }
}