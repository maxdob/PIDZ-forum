const colors = {
    mainblue: '#28518D',
    white: '#fff',
    lightblue: '#EEF7FF',
    dark: '#292929',
    gray: '#787878'
}

export default colors;