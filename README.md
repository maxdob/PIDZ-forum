# PIDZ vragen forum

This is a school project commissioned for PIDZ. I've developed a forum for the freelancers who are associated with PIDZ. In the forum you can ask en answer questions.

## Prerequisites

You'll be needing te run the project

```
NPM 
Node.JS
```

## Built With

I've built the project with the MERN stack method.

- MongoDB. Datebase
- Express. Routing the DB to front-end
- Node JS. Used to run javascript and outside the browser
- React JS. JavaScript library for building interfaces 

## Folder info

### /Backend

This file consists of REST API calls to the MongoDB. If you want to change the DB you can easily change the routes there.
The models are the skeleton of an object that you want to add, change, read or delete in the database.

### /src

The app.js file contains the different routes of the page. In the /component folder are the different components that are displayed in the main app.js file. 
I've created it in the React way so it's all run on a single page.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### 'nodemon server'

First navigate to the /backed folder and then run this script. this will make sure all the API calls can be accesed and connect to the databas.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

