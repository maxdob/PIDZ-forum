const router = require('express').Router();
let Vraag = require('../models/vraag.model');

router.route('/').get((req, res) => {
  Vraag.find()
    .then(vragen => res.json(vragen))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const name = req.body.name;
  const title = req.body.title;
  const description = req.body.description;
  const date = Date.parse(req.body.date);
  const anonymous = req.body.anonymous;
  const views = req.body.views;

  const newVraag = new Vraag({
    name,
    title,
    description,
    date,
    anonymous,
    views,
  });

  newVraag.save()
  .then(() => res.json('vraag added!'))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Vraag.findById(req.params.id)
    .then(vraag => res.json(vraag))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Vraag.findByIdAndDelete(req.params.id)
    .then(() => res.json('vraag deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
  Vraag.findById(req.params.id)
    .then(vraag => {
      vraag.name = req.body.name;
      vraag.title = req.body.title;
      vraag.description = req.body.description;
      vraag.date = Date.parse(req.body.date);
      vraag.anonymous = req.body.anonymous;
      vraag.views = req.body.views;
      vraag.comments = req.body.comments;


      vraag.save()
        .then(() => res.json('Vraag updated!'))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;