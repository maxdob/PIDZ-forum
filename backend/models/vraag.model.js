const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const vraagSchema = new Schema({
    name: { type: String, required: true},
    title: {type: String,  required: true},
    description: {type: String,  required: true},
    date: {type: String,  required: true},
    anonymous: {type: Boolean, required: true},
    views: {type: Intl, required: false},
    comments: {type: Array, required: false}
},{ 
    timestamps: true,
});

const Vraag = mongoose.model('Vraag', vraagSchema);

module.exports = Vraag;