const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    voornaam: {type: String, required: true, unique: true, trim: true},
    achternaam: {type: String, required: true, unique: true, trim: true},
    geslacht: {type: String, required: true},
},{ 
    timestamps: true,
});

const User = mongoose.model('User', userSchema);

module.exports = User;